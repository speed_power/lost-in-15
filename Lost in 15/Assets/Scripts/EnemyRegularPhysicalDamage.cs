﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRegularPhysicalDamage : MonoBehaviour
{
    private int regularPhysicalDamage;
    // Use this for initialization
    void Start()
    {
        regularPhysicalDamage = 5;
    }

    int GetRegularPhysicalDamage()
    {
        return regularPhysicalDamage;
    }
}
