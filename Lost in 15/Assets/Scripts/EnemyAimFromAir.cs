﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAimFromAir : MonoBehaviour
{

    Vector3 myPos;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 player = CharactersManager.GetInstance().Player.transform.position;
        myPos = this.transform.position;
        Vector3 dir = player - myPos;
        //dir.y = 0.0f;
        dir.Normalize();

        this.transform.rotation = Quaternion.LookRotation(dir, Vector3.up);

    }
}
