﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float JumpSpeed = 15.0f;
    public float MoveSpeed = 5.0f;
    public float Gravity = 18.0f;
    public bool damageJumpDone = true;
	private PlayerSoundAudios audio;

    enum States
    {
        Normal,
        StartImpact,
        ImpactActive
    }

    States currentState = States.Normal;

    Animator animator;

    CharacterController controller;
    public Vector3 velocity = Vector3.zero;
    Vector3 impactFlyDirection = Vector3.zero;
    PlayerRecoilBool recoil;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
        recoil = PlayerRecoilBool.GetInstance();
		audio = GetComponent<PlayerSoundAudios> ();
    }

    // Update is called once per frame
    void Update()
    {

        switch (currentState)
        {
            case States.Normal:
                StateNormal();
                break;
            case States.StartImpact:
                StateStartImpact();
                break;
            case States.ImpactActive:
                StateImpactActive();
                break;
        }

        velocity.y -= Gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }

    void StateNormal()
    {
        float vertical = Input.GetAxis("Vertical") * MoveSpeed;
        float horizontal = Input.GetAxis("Horizontal") * MoveSpeed;
        velocity.x = 0.0f;
        velocity.z = 0.0f;

        velocity.x += this.transform.forward.x * vertical;
        velocity.z += this.transform.forward.z * vertical;

        velocity.x += this.transform.right.x * horizontal;
        velocity.z += this.transform.right.z * horizontal;

        if (controller.isGrounded)
        {
            animator.SetFloat("Strafe", horizontal);
            animator.SetFloat("Speed", vertical);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (vertical >= 0)
                    animator.SetTrigger("Flip");
                else
                    animator.SetTrigger("BackFlip");

                velocity.y = JumpSpeed;
				audio.PlayJump ();

            }
            else
            {
                velocity.y = 0.0f;
            }
        }
        else
        {
            animator.SetFloat("Strafe", 0);
            animator.SetFloat("Speed", 0);
        }
        recoil.SetRecoil(false);

    }

    void StateStartImpact()
    {
        velocity = impactFlyDirection;
        Debug.Log(impactFlyDirection);
        velocity.y = 10; //impactFlyDirection.y
        currentState = States.ImpactActive;
        recoil.SetRecoil(true);
		audio.Playdamage ();
    }

    void StateImpactActive()
    {
        if (controller.isGrounded)
        {
            currentState = States.Normal;
        }
    }

    public Vector3 GetVelocity()
    {
        return velocity;
    }
    public void SetImpactFlyDirection(Vector3 _impactFlyDirection)
    {
        impactFlyDirection = _impactFlyDirection;
        currentState = States.StartImpact;
    }

}