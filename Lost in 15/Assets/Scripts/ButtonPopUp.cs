﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonPopUp : MonoBehaviour
{
    Text popUpText;
    void Start()
    {
        popUpText = GetComponentInChildren<Text>();
        popUpText.text = "";
    }
    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("ENTRO");
        if (collider.gameObject.tag == "JumpPopUpArea")
        {
            popUpText.text = "Press SPACE to jump.";
        }
    }
    public void EnterJumpText()
    {
        popUpText.text = "Press SPACE to jump.";
    }
    public void EnterShotText()
    {
        popUpText.text = "Left click to shoot!";
    }
    public void EnterMoveAndLookText()
    {
        popUpText.text = "Use WASD keys to run, and move the mouse to look around";
    }
    public void ClearText()
    {
        popUpText.text = "";
    }
	void Update ()
    {
    }
}
