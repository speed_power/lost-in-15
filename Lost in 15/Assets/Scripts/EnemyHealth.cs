﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField]
    UnityEvent death;

    [SerializeField]
    GameObject ragdoll;

	private PlayerSoundAudios audio;
	void Start()
	{
		audio = GetComponent<PlayerSoundAudios> ();
	}
    public delegate void DieDlg(EnemyHealth enemy);

    const int maxHealth = 100;

    [SerializeField]
    private int currentHealth = maxHealth;

    List<DieDlg> dieCallback = new List<DieDlg>();

    void Update()
    {
		if (currentHealth <= 0) {
			audio.PlayJump ();
			Death ();
		}

    }

    public void AddListener(DieDlg callback)
    {
        dieCallback.Add(callback);
    }

    public void RemoveListener(DieDlg callback)
    {
        if (dieCallback.Contains(callback))
            dieCallback.Remove(callback);
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Bullet")
        {
            currentHealth -= collider.gameObject.GetComponent<Bullet>().GetDamage();
            if (currentHealth <= 0)
                InvokeDieCallbacks();
           // collider.gameObject.GetComponent<Bullet>().Recycle();
            PoolObject po = collider.GetComponent<PoolObject>();
            po.Recycle();
            //Destroy(collider.gameObject);
            Debug.Log("Enemigo recibio danio");
        }
    }

    private void InvokeDieCallbacks()
    {
        foreach (DieDlg cbk in dieCallback)
            cbk(this);
    }


    void Death()
    {
        death.Invoke();

        Instantiate<GameObject>(ragdoll, transform.position, transform.rotation);

        Destroy(gameObject);
    }
}