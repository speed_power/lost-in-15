﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegularPhysicalDamage : MonoBehaviour {

    private int regularPhysicalDamage;
    // Use this for initialization
    void Start()
    {
        regularPhysicalDamage = 5;
    }

    public int GetRegularPhysicalDamage()
    {
        return regularPhysicalDamage;
    }
}
