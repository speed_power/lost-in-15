﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
	[SerializeField]
	float jumpForce;
	float jumpHeight;
	[SerializeField]
	float jumpMaxHeight;
	bool jumpEnabled;
	bool grounded = false;
	[SerializeField]
	int weight;
	public Rigidbody rb;
    void Start ()
	{
		rb = GetComponent<Rigidbody> ();
		jumpHeight = 0;
		jumpEnabled = true;
	}
	void Update ()
	{
		/*if (GetComponent<BoxCollider> ().tag == "Floor")
		{

		}*/
		if (Input.GetKey ("space") && jumpEnabled == true) {
			rb.velocity = new Vector3 (0, jumpForce, 0);
			jumpHeight++;
			grounded = false;
		}
		else if (!grounded)
		{
			jumpEnabled = false;
		}
		else
			jumpEnabled = true;
		
		if (jumpHeight > jumpMaxHeight)
			jumpEnabled = false;
		//if (Input.GetKeyUp ("space"))
		//	jumpEnabled = false;
		rb.AddForce(weight * Physics.gravity);
	}
	private void OnCollisionEnter(Collision something)
	{
		if (something.gameObject.tag == "Floor") {
			jumpHeight = 0;
			jumpEnabled = true;
			grounded = true;
			Debug.Log ("Salto encendido");
		}
	}
		
}

/*
 * 
*/