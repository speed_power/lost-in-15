﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundAudios : MonoBehaviour {


	private AudioSource AudioPlayer;
	public AudioClip damage; //set this in ispector with audiofile
	public AudioClip jump; //set this in ispector with audiofile

	void Awake()
	{
		AudioPlayer = GetComponent<AudioSource>();

	}

	public void Playdamage()
	{
		AudioPlayer.clip = damage;
		AudioPlayer.Play();
		Debug.Log("Playing sound Damage");
	}
	public void PlayJump()
	{
		AudioPlayer.clip = jump;
		AudioPlayer.Play();
		Debug.Log("Playing sound Jump");
	}
	}
