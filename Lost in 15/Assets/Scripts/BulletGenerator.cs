﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletGenerator : MonoBehaviour
{

    [SerializeField]
    Transform m_Camera;

    [SerializeField]
    private Text HUD;

    [SerializeField]
    GameObject Bullet;

    [SerializeField]
    LayerMask layers;

    [SerializeField]
    Transform bulletSpawn;

    const int maxAmmo = 100;

    [SerializeField]
    private int currentAmmo = maxAmmo;

    AudioSource disparo;

    GameObject clone;
    Pool bulletPool;
    
    void Start()
    {
        ShowCurrentAmmo();
        bulletPool = PoolManager.GetInstance().GetPool("BulletPool");
        disparo = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (currentAmmo>0)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && Time.timeScale!=0)
            {
                if (disparo)
                    disparo.Play();

                RaycastHit hit;

                Ray ray = new Ray(m_Camera.transform.position, m_Camera.transform.forward);

                if (Physics.Raycast(ray, out hit, Mathf.Infinity, layers))
                {
                    PoolObject po = bulletPool.GetPooledObject();

                    po.transform.position = bulletSpawn.position;
                    Vector3 dir = (hit.point - bulletSpawn.position).normalized;
                    po.transform.forward = dir;
                    //po.transform.rotation = transform.rotation;

                    currentAmmo--;
                    ShowCurrentAmmo();
                }
                else
                {
                    PoolObject po = bulletPool.GetPooledObject();
                    po.transform.position = bulletSpawn.position;
                    po.transform.rotation = transform.rotation;

                    currentAmmo--;
                    ShowCurrentAmmo();
                }

            }
        }
    }
	public void AddAmmo(int ammo)
	{
		if ((ammo + currentAmmo) > maxAmmo)
			currentAmmo = maxAmmo;
		else 
		{
			currentAmmo += ammo;
			ShowCurrentAmmo ();
		}
		Debug.Log("Estoy en AddAmmo");
	}

    public void ShowCurrentAmmo()
    {
        HUD.text = currentAmmo.ToString() + "/" + maxAmmo.ToString();
    }

    public bool AmmoIsFull()
    {
        if (currentAmmo == maxAmmo)
            return true;

        return false;
    }
}