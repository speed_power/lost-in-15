﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyBulletGenerator : MonoBehaviour
{
    [SerializeField]
    GameObject EnemyBullet;
	private PlayerSoundAudios audio;

    GameObject clone;
    Pool enemyBulletPool;

   [SerializeField]
    private float waitToShootTimer;
    private float savedTimerReset;

    void Start()
    {
		audio = GetComponent<PlayerSoundAudios> ();

        if (GetComponent<EnemyFoward>().GetEnemyType() == EnemyType.Archer)
        {
            enemyBulletPool = PoolManager.GetInstance().GetPool("enemyBulletPool2");
        }
        else if (GetComponent<EnemyFoward>().GetEnemyType() == EnemyType.Wizard)
        {
            enemyBulletPool = PoolManager.GetInstance().GetPool("enemyBulletPool");
        }

        
        savedTimerReset = waitToShootTimer;
        waitToShootTimer = 0.0f;
    }

    public void Shoot()
    {
        waitToShootTimer -= Time.deltaTime;
            if (waitToShootTimer <= 0)
            {
				audio.Playdamage ();
                PoolObject enemyPo = enemyBulletPool.GetPooledObject();
                enemyPo.transform.position = transform.position;
                enemyPo.transform.rotation = transform.rotation;
                waitToShootTimer = savedTimerReset;
            }
     }
}

