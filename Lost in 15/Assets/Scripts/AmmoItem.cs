﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AmmoItem : MonoBehaviour
{
    bool active = true;
    MeshRenderer theRender;
    [SerializeField]
    private int ammoAmount;

    [SerializeField]
    float countdownValue;

    [SerializeField]
    float currCountdownValue;
    AudioSource pickUpSound;

    BoxRenderer theBoxRenderer;
    void Start()
    {
        pickUpSound = GetComponent<AudioSource>();
        //theRender = GetComponent<MeshRenderer>();
        theBoxRenderer = GetComponentInChildren<BoxRenderer>();
    }
    public int GetAmmoAmmount()
    {
        return ammoAmount;
    }
    void Update()
    {
        
        if (!active)
        {
            currCountdownValue -= 1 * Time.deltaTime;
            if (currCountdownValue <= 0)
            {
                active = true;
                GetComponent<BoxCollider>().enabled = true;
                //theRender.enabled = true;
                theBoxRenderer.MeshOn(true);
            }
                
        }
    }

    public void SetInactive()
    {
        active = false;
        GetComponent<BoxCollider>().enabled = false;
        //theRender.enabled = false;
        theBoxRenderer.MeshOn(false);
        Debug.Log("Ammo se borra?");

    }
    public void PlayPickUpSound()
    {
        pickUpSound.Play();
        Debug.Log("PLAY SOUND");
    }

}
