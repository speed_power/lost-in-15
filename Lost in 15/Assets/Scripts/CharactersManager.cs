﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactersManager : MonoBehaviour
{
    public GameObject Player;

    private static CharactersManager instance = null;

    public static CharactersManager GetInstance()
    {
        if (instance == null)
            instance = FindObjectOfType<CharactersManager>();
        return instance;
    }

    void Awake()
    {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
