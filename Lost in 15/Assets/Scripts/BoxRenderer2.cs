﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxRenderer2 : MonoBehaviour
{
    MeshRenderer theRenderer;
    void Start()
    {
        theRenderer = GetComponent<MeshRenderer>();
    }
    public void MeshOn(bool on)
    {
        if (on)
            theRenderer.enabled = true;
        else
            theRenderer.enabled = false;
    }
}
