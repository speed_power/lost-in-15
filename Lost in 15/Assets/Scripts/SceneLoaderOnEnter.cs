﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoaderOnEnter : MonoBehaviour
{
    [SerializeField] Slider loadingBar;
    [SerializeField] GameObject loadingCanvas;

    public string SceneToLoad = "Game";

    IEnumerator LoadAsynchronously()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(SceneToLoad);

        while (!operation.isDone)
        {
            loadingCanvas.SetActive(true);

            float progress = Mathf.Clamp01(operation.progress / .9f);
            loadingBar.value = progress;

            yield return null;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag=="Player")
            LoadScene();
    }

    public void LoadScene()
    {
        GameData.CurrentLevel = 2;
        StartCoroutine(LoadAsynchronously());
    }
}
