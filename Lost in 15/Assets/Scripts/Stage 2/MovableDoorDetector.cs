﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableDoorDetector : MonoBehaviour
{
    MovableDoor thisDoor;
	void Start ()
    {
        thisDoor = GetComponentInParent<MovableDoor>();
	}
	void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            thisDoor.StartClosing();
        }
    }
	void Update ()
    {
		
	}
}
