﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableDoor : MonoBehaviour
{
    [SerializeField]
    float speed;
    bool movingDown;
    bool jobDone;
	void Start ()
    {
        movingDown = false;
        jobDone = false;
	}
    public void StartClosing()
    {
        if(!jobDone)
            movingDown = true;
    }
    public void StopClosing()
    {
        movingDown = false;
        jobDone = true;
    }
    public void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == "FloorForMovableDoor")
        {
            StopClosing();
        }
    }
	void Update ()
    {
		if(movingDown)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - (speed * Time.deltaTime), transform.position.z);
            //Debug.Log("TRASLADANDO");
        }
	}
}
