﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFoward : MonoBehaviour
{
    [SerializeField]
    GameObject player;
    [SerializeField]
    float maxAproachRadio;
    [SerializeField]
    float minAproachRadio;
    [SerializeField]
    float closeSpeed;
    [SerializeField]
    float farSpeed;
    [SerializeField]
    EnemyType type;
    [SerializeField]
    LayerMask layers;

    bool enemyDetected;
    PlayerRecoilBool recoil;
    EnemyBehaviour enemyBehaviour;

    void Start()
    {
        recoil = PlayerRecoilBool.GetInstance();
    }

    private void Awake()
    {
        switch (type)
        {
            case EnemyType.Wizard:
                enemyBehaviour = gameObject.AddComponent<WizardBehaviour>();
                break;
            case EnemyType.Melee:
                enemyBehaviour = gameObject.AddComponent<MeleeBehaviour>();
                break;
            case EnemyType.Archer:
                enemyBehaviour = gameObject.AddComponent<ArcherBehaviour>();
                break;
            default:
                enemyBehaviour = gameObject.AddComponent<MeleeBehaviour>();
                break;
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    void Update()
    {
        if(recoil.GetRecoil() == false)
        {
            Vector3 distance = player.transform.position - transform.position;
            float magnitude = distance.magnitude;

            if (magnitude < maxAproachRadio)
            {
                RaycastHit hit = new RaycastHit();
                Vector3 playerPoint = player.transform.position - transform.position;
                Ray ray = new Ray(transform.position, playerPoint);

                if (Physics.Raycast(ray, out hit, playerPoint.magnitude, layers)
                  && hit.collider.gameObject.CompareTag("Player"))
                {
                    if (magnitude < minAproachRadio)
                        enemyBehaviour.PlayerOnSight(closeSpeed);
                    else if (magnitude < maxAproachRadio && magnitude > minAproachRadio)
                        enemyBehaviour.PlayerOnSight(farSpeed);
                }
                else
                    enemyBehaviour.PlayerOutOfSight();
            }
            else
                enemyBehaviour.PlayerOutOfSight();

        }
    }

    public EnemyType GetEnemyType()
    {
        return type;
    }
}
