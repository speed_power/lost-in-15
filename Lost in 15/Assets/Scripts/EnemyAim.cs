﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAim : MonoBehaviour
{

    Vector3 myPos;
    Animator anim;
    float coolDown = 0.5f;
    void Start ()
    {
        anim = GetComponent<Animator>();
    }
    private void OnCollisionEnter(Collision something)
    {
        if (something.gameObject.tag == "Floor")
        {
            //anim.
            if(anim.GetBool("Attacking"))
            {
                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                {
                    anim.SetBool("Attacking", false);
                }
            }
        }
    }
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            anim.SetBool("Attacking", true);
        }
    }
    void Update ()
    {
        Vector3 player = CharactersManager.GetInstance().Player.transform.position;
        myPos = this.transform.position;
        Vector3 dir = player - myPos;
        dir.y = 0.0f;
        dir.Normalize();
        if (anim.GetBool("Attacking"))
        {
            coolDown -= Time.deltaTime;
            if (coolDown <= 0.0f)
            {
                coolDown =  0.5f;
                anim.SetBool("Attacking", false);
            }
        }
        this.transform.rotation = Quaternion.LookRotation(dir, Vector3.up);
        
    }
}
