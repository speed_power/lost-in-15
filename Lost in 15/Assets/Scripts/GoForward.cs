﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoForward : MonoBehaviour {

    [SerializeField]
    float velocity = 1;

	void Update () {
        this.transform.Translate(0,0,velocity*Time.deltaTime);
	}
}
