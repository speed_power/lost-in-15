﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObject : MonoBehaviour
{
	private Pool pool;
	// Use this for initialization
	void Start ()
	{
		
	}
	public void SetPool(Pool comingPool)
	{
		pool = comingPool;
	}
	public void Recycle()
	{
		pool.Recycle(this);
	}
}
