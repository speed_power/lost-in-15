﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

	[SerializeField]
	private float m_RevolutionTime = 1f;
	[SerializeField]
	private Transform m_PickupObjectTransform;

	// Update is called once per frame
	void Update () {
		float angles = 360f / m_RevolutionTime;
		m_PickupObjectTransform.Rotate (Vector3.up, angles * Time.deltaTime);
	}
}
