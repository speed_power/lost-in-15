﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{

    [SerializeField]
    private float velBullet;

    [SerializeField]
    private int damage;

    private float timer = 0.0f;

    [SerializeField]
    float timeToExist;

    public int GetDamage()
    {
        return damage;
    }

    private void OnEnable()
    {
        timer = timeToExist;
    }

    void Update()
    {
        transform.Translate(0, 0, velBullet * Time.deltaTime);

        timer -= Time.deltaTime;

        if (timer <= 0.0f)
        {
            Recycle();
        }
    }
    public void Recycle()
    {
        PoolObject po = GetComponent<PoolObject>();
        po.Recycle();
    }
}
