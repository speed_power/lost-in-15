﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCollisions : MonoBehaviour
{
	
    float mass = 3.0F;
    Vector3 lastEnemyDirection;
    Vector3 flyDirection;

    private CharacterController characterCon; //CharacterController
    private CharacterMovement charMove;

    [SerializeField]
    float impactForce;

    bool partCollected = false;

    [SerializeField]
    RectTransform healthBar;

    Vector2 healthBarSize;

    const int maxHealth = 100;

    [SerializeField]
    string SceneToLoad = "GameOver";

    [SerializeField]
    private int currentHealth = maxHealth;

    RegularPhysicalDamage regularDamage;
    int cantidad;

    int currentLevel;

    BulletGenerator bg;
    ButtonPopUp buttonPopUp;

    private void Start()
    {
        regularDamage = GetComponent<RegularPhysicalDamage>();
        characterCon = GetComponent<CharacterController>();
        charMove = GetComponent<CharacterMovement>();
        currentLevel = GameData.CurrentLevel;
        buttonPopUp = GetComponentInChildren<ButtonPopUp>();
    }

    void Update()
    {
        if (currentHealth <= 0)
        {
            GameData.CurrentLevel = currentLevel;
            Death();
        }
    }

    void Awake()
    {
        bg = GetComponentInChildren<BulletGenerator>();
        healthBarSize = healthBar.sizeDelta;
        UpdateCurrentHealthHUD();
    }

    private void OnTriggerEnter(Collider collider)
    {


        if (collider.gameObject.tag == "EnemyBullet")
        {
            EnemyBullet eb = collider.gameObject.GetComponent<EnemyBullet>();
            currentHealth -= eb.GetDamage();
            eb.Recycle();
        }
        if (collider.gameObject.tag == "ShipPart")
        {
            partCollected = true;
            Destroy(collider.gameObject);
        }

        if (collider.gameObject.tag == "Ship" && partCollected)
        {
            SceneManager.LoadScene(0);
        }
        if (collider.gameObject.tag == "Instakill")
        {
            currentHealth -= currentHealth;
        }
        if (collider.gameObject.tag == "AmmoItem")
        {
            Debug.Log("COLISION CON AMMOITEM");
            if (!bg.AmmoIsFull())
            {
                cantidad = collider.gameObject.GetComponent<AmmoItem>().GetAmmoAmmount();
                bg.AddAmmo(cantidad);
                bg.ShowCurrentAmmo();
                collider.gameObject.GetComponent<AmmoItem>().PlayPickUpSound();
                collider.gameObject.GetComponent<AmmoItem>().SetInactive();
            }
        }
        if (collider.gameObject.tag == "JumpPopUpArea")
        {
            buttonPopUp.EnterJumpText();
        }
        if (collider.gameObject.tag == "ShotPopUpArea")
        {
            buttonPopUp.EnterShotText();
        }
        if (collider.gameObject.tag == "MoveAndLookPopUpArea")
        {
            buttonPopUp.EnterMoveAndLookText();
        }
        if (collider.gameObject.tag == "Enemy")
        {
            //Addimpact();
            //BackJump();
            EnemyMeleeBehavior melee = collider.gameObject.GetComponent<EnemyMeleeBehavior>();
            EnemyFoward enemyPosition = collider.gameObject.GetComponent<EnemyFoward>();
            CalculeEnemyDirection(enemyPosition.GetPosition());
            BackJump(flyDirection);
            currentHealth -= regularDamage.GetRegularPhysicalDamage();



            if (melee != null && melee.IsAttacking())
            {
                currentHealth -= melee.GetDamage();
                melee.StartCoolDown();
            }
        }
        UpdateCurrentHealthHUD();
    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "JumpPopUpArea" || collider.gameObject.tag == "ShotPopUpArea" || collider.gameObject.tag == "MoveAndLookPopUpArea")
        {
            buttonPopUp.ClearText();
        }
    }
    private void CalculeEnemyDirection(Vector3 _enemyPosition)
    {
        lastEnemyDirection = (_enemyPosition - this.transform.position).normalized;
        flyDirection = new Vector3(-lastEnemyDirection.x * impactForce, 0, -lastEnemyDirection.z * impactForce);
    }
    private void BackJump(Vector3 enemyDirection)
    {
        charMove.SetImpactFlyDirection(flyDirection);
    }
    void Death()
    {
        SceneManager.LoadScene(SceneToLoad);
    }

    void UpdateCurrentHealthHUD()
    {
        healthBar.sizeDelta = new Vector2(currentHealth * healthBarSize.x / maxHealth, healthBarSize.y);
    }
}