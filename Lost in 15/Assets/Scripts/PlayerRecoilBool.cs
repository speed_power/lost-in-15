﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRecoilBool : MonoBehaviour
{
    private static PlayerRecoilBool instance = null;
    bool playerInRecoil = false;
    public static PlayerRecoilBool GetInstance()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<PlayerRecoilBool>();
        }
        return instance;
    }
    public void SetRecoil(bool recoil)
    {
        playerInRecoil = recoil;
    }
    public bool GetRecoil()
    {
        return playerInRecoil;
    }
}
