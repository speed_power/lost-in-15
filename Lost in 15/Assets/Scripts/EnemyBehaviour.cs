﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBehaviour : MonoBehaviour
{
    [SerializeField]
    protected float maxAproachRadio;
    [SerializeField]
    protected float minAproachRadio;
    [SerializeField]
    protected GameObject player;
    [SerializeField]
    EnemyType type;
    public abstract void PlayerOnSight(float speed);
    public abstract void PlayerOutOfSight();

    protected Animator anim;
    protected EnemyBulletGenerator bulletGenerator;
}

public class WizardBehaviour : EnemyBehaviour
{
    void Start()
    {
        bulletGenerator = GetComponent<EnemyBulletGenerator>();
        anim = GetComponent<Animator>();
    }

    public override void PlayerOnSight(float speed)
    {
        bulletGenerator.Shoot();
        if (!anim.GetBool("Attacking"))
            anim.SetBool("Attacking", true);
    }

    public override void PlayerOutOfSight()
    {
        if (anim.GetBool("Attacking"))
            anim.SetBool("Attacking", false);
    }
}

public class MeleeBehaviour : EnemyBehaviour
{
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public override void PlayerOnSight(float speed)
    {
        anim.SetFloat("Speed", speed);

            transform.position += transform.forward * speed * Time.deltaTime;

    }

    public override void PlayerOutOfSight()
    {
        anim.SetFloat("Speed", 0.0f);
    }
}

public class ArcherBehaviour : EnemyBehaviour
{
    void Start()
    {
        bulletGenerator = GetComponent<EnemyBulletGenerator>();
        anim = GetComponent<Animator>();
    }

    public override void PlayerOnSight(float speed)
    {
        bulletGenerator.Shoot();

        anim.SetFloat("Speed", speed);

        transform.position += transform.forward * speed * Time.deltaTime;
    }

    public override void PlayerOutOfSight()
    {
        anim.SetFloat("Speed", 0.0f);
    }
}

public enum EnemyType
{
    Wizard,Melee,Archer
};