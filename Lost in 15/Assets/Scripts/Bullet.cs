﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    [SerializeField]
    private float velBullet;

    [SerializeField]
    private int damage;

    private float timer = 0.0f;

    public int GetDamage()
    {
        return damage;
    }

    private void OnEnable()
    {
        timer = 3.0f;
    }
    
    void Update () {
        transform.Translate(0, 0, velBullet * Time.deltaTime);

        timer -= Time.deltaTime;

        if (timer <= 0.0f)
        {
            PoolObject po = GetComponent<PoolObject>();
            po.Recycle();
        }
	}
}
