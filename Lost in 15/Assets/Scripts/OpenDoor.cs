﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{

    float currentRotationX;

    bool open = false;
    bool opening = false;
    AudioSource audioSource;

    [SerializeField]
    List<EnemyHealth> enemys;
    [SerializeField]
    AudioClip openingGateSound;
    [SerializeField]
    AudioClip gateOpenSound;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Awake()
    {
        currentRotationX = transform.rotation.x;

        foreach (EnemyHealth enemy in enemys)
        {
            enemy.AddListener(OnEnemyDie);
        }
    }

    void OnEnemyDie(EnemyHealth enemy)
    {
        enemys.Remove(enemy);
    }

    private void Update()
    {
        Vector3 angle = transform.rotation.eulerAngles;

        if (Time.timeScale != 0)
        {
            if (open && currentRotationX + 90 > angle.x)
            {
                transform.Rotate(0.4f, 0, 0);

                if (!audioSource.isPlaying)
                    audioSource.PlayOneShot(openingGateSound);

                if (!opening)
                    opening = true;
            }
            else if (open && currentRotationX + 90 == angle.x && opening)
            {
                opening = false;

                if (audioSource.isPlaying)
                    audioSource.Stop();

                audioSource.PlayOneShot(gateOpenSound);
            }
        }
        else
        {
            audioSource.Stop();
        }


        if (enemys.Count == 0)
        {
            open = true;
        }
    }
}
