﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
	public GameObject Prefab;
	public int count = 100;
	private List<PoolObject> poolList = new List<PoolObject> ();

	void Awake ()
	{
		for (int i = 0; i < count; i++) 
		{
			PoolObject po = CreateObject();
			poolList.Add(po);
		}
	}
	public PoolObject GetPooledObject()
	{
		if (poolList.Count > 0) {
			PoolObject po = poolList [0];
			po.gameObject.SetActive (true);
			poolList.RemoveAt (0);
			return po;
		} 
		else 
		{
			PoolObject po = CreateObject ();
			po.gameObject.SetActive (true);
			Debug.LogWarning ("Se creo un objeto en tiempo de ejecucion (pool)");
			return po;
		}
	}
	public void Recycle(PoolObject po)
	{
		po.gameObject.SetActive (false);
		if (!poolList.Contains (po))
			poolList.Add (po);
	}
	private PoolObject CreateObject()
	{
		GameObject go = Instantiate(Prefab);

		PoolObject po = go.AddComponent<PoolObject> ();

		//go.AddComponent<PoolObject>();
		//PoolObject po = go.GetComponent<PoolObject> ();
		//Esta es otra forma de hacerlo. Es igual. Se entiende mejor pero ocupa dos lineas de codigo.

		po.SetPool (this);
		go.SetActive(false);

		return po;
	}
	// Update is called once per frame
	void Update ()
	{
		
	}
}
