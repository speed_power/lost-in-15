﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleeBehavior : MonoBehaviour
{
    [SerializeField]
    float recoilDistance;
    [SerializeField]
    public int damage;
    [SerializeField]
    float jumpForce;
    float jumpHeight;
    [SerializeField]
    float jumpMaxHeight;
    bool jumpEnabled;
    bool grounded = false;
    [SerializeField]
    int weight;
    public Rigidbody rb;
    float coolDown = 0.5f;
    bool isAttacking = true;
    bool hitting;
    PlayerRecoilBool recoil;
    Animator anim;
	PlayerSoundAudios audio;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jumpHeight = 0;
        jumpEnabled = true;
        hitting = false;
        anim = GetComponent<Animator>();
		audio = GetComponent<PlayerSoundAudios> ();
    }
    // Update is called once per frame
    void Update()
    {
        if (!isAttacking)
        {
            coolDown -= Time.deltaTime;
            if (coolDown <= 0.0f)
            {
                coolDown = 0.0f;
                isAttacking = true;
            }
        }
        //Debug.Log(anim.GetBool("Attacking"));
    }
    public int GetDamage()
    {
        return damage;
    }

    public void StartCoolDown()
    {
        coolDown = 0.5f;
        isAttacking = false;
    }
    public bool IsAttacking()
    {
        return isAttacking;
    }
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
			audio.Playdamage ();
            anim.SetBool("Attacking", true);
            BackJump();
        }
    }
    private void BackJump()
    {
        //transform.Translate(new Vector3(0, 0, -recoilDistance));
        if (jumpEnabled == true)
        {
            rb.velocity = new Vector3(0, jumpForce, 0) - transform.forward * recoilDistance;
            jumpHeight++;
            grounded = false;
        }
        else if (!grounded)
        {
            jumpEnabled = false;
        }
        else
            jumpEnabled = true;

        if (jumpHeight > jumpMaxHeight)
            jumpEnabled = false;
        //if (Input.GetKeyUp ("space"))
        //	jumpEnabled = false;
        rb.AddForce(weight * Physics.gravity);
    }
    private void OnCollisionEnter(Collision something)
    {
        if (something.gameObject.tag == "Floor")
        {
            anim.SetBool("Attacking", false);
            jumpHeight = 0;
            jumpEnabled = true;
            grounded = true;
            //Debug.Log("Salto enemigoMelee encendido");
        }
    }

}


/*
    void Update()
    {
        /*if (GetComponent<BoxCollider> ().tag == "Floor")
		{

		}
        if (Input.GetKey("space") && jumpEnabled == true)
        {
            rb.velocity = new Vector3(0, jumpForce, 0);
jumpHeight++;
            grounded = false;
        }
        else if (!grounded)
        {
            jumpEnabled = false;
        }
        else
            jumpEnabled = true;

        if (jumpHeight > jumpMaxHeight)
            jumpEnabled = false;
        //if (Input.GetKeyUp ("space"))
        //	jumpEnabled = false;
        rb.AddForce(weight* Physics.gravity);
    }
    private void OnCollisionEnter(Collision something)
{
    if (something.gameObject.tag == "Floor")
    {
        jumpHeight = 0;
        jumpEnabled = true;
        grounded = true;
        Debug.Log("Salto enemigoMelee encendido");
    }
}


  */