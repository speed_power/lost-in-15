﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SkipIntro : MonoBehaviour
{

    [SerializeField] UnityEvent OnEnter;
    [SerializeField] Slider loadingBar;
    [SerializeField] GameObject loadingCanvas;

    bool playingIntro = false;
    public string SceneToLoad = "Game";

   

    void Update()
    {

        if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) && playingIntro)
        {
            OnEnter.Invoke();
            StartCoroutine(LoadAsynchronously());
        }
    }

    IEnumerator LoadAsynchronously()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(SceneToLoad);

        while (!operation.isDone)
        {
            loadingCanvas.SetActive(true);

            float progress = Mathf.Clamp01(operation.progress / .9f);
            loadingBar.value = progress;

            yield return null;
        }
    }

    public bool IsPlaying()
    {
        return playingIntro;
    }

    public void PlayIntro()
    {
        playingIntro = true;
    }
}
