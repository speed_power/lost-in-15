﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadSceneOnClick : MonoBehaviour
{
    [SerializeField]
    public string SceneToLoad;
    Button button;
    [SerializeField]
    bool isMenuButton;
    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(LoadScene);
    }

    public void LoadScene()
    {
        if(!isMenuButton)
        {
            if(GameData.CurrentLevel == 1)
                SceneManager.LoadScene("Test Scene");
            else
                SceneManager.LoadScene("Test Scene 1");
        }
        else
            SceneManager.LoadScene(SceneToLoad);
    }

}
