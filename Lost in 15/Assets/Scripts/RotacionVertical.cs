﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionVertical : MonoBehaviour
{
	float verticalRotation = 0.0f;
	[SerializeField]
	float min = -25.0f;
	[SerializeField]
	float max = 25.0f;
	float angle = 0.0f;
    [SerializeField]
    float sensibility;

    void Update()
    {
        if (Time.timeScale!=0)
        {
            verticalRotation = -Input.GetAxis("Mouse Y");
            angle += (verticalRotation * sensibility);

            if (angle > max)
            {
                angle = max;
            }
            else if (angle < min)
            {
                angle = min;
            }


            Quaternion originalRot = transform.localRotation;
            Vector3 euler = originalRot.eulerAngles;
            euler.x = 0.0f;
            originalRot.eulerAngles = euler;
            transform.localRotation = originalRot * Quaternion.AngleAxis(angle, Vector3.right);

            /*if (transform.eulerAngles.y > 90.0f) {
                Vector3 euler = transform.eulerAngles;
                euler.y = 90.0f;
                transform.eulerAngles = euler;
            }*/
        }
    }
}