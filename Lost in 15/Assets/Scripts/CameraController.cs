﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    LayerMask layers;
    [SerializeField]
    Transform playerCamera;
    [SerializeField]
    Transform farPosition;
    [SerializeField]
    Transform closePosition;

    void Update()
    {

        RaycastHit hit;
        Ray ray = new Ray(closePosition.position, farPosition.position - closePosition.position);
        //Debug.DrawRay(closePosition.position, farPosition.position - closePosition.position, Color.red, 1f);

        if (Physics.Raycast(ray, out hit, (farPosition.position - closePosition.position).magnitude, layers))
            playerCamera.position = hit.point + hit.normal.normalized *0.1f;
        else
            if (farPosition.position != playerCamera.position)
            playerCamera.position = farPosition.position;
    }
}
