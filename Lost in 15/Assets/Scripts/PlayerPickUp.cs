﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickUp : MonoBehaviour
{

    bool active = true;
    [SerializeField]
    float countdownValue;

    [SerializeField]
    float currCountdownValue;

    [SerializeField]
	private float m_Health= 100f;
	[SerializeField]
	private float m_HealthIncrement = 10f;
	private float m_MaxHealth= 100f;

	[SerializeField]
	private float m_Ammo= 100f;
	[SerializeField]
	private float m_AmmoIncrement = 5f;
	private float m_MaxAmmo= 25f;

	private void OnTriggerEnter (Collider collider)
	{
	if (collider.gameObject.tag == "PickUpHealth")
		{
			Debug.Log (m_Health);
		if (m_Health < m_MaxHealth)
		{
			m_Health += m_HealthIncrement;
				Debug.Log (m_Health);
			Destroy (collider.gameObject);
		}
		if (m_Health > m_MaxHealth)
			m_Health = m_MaxHealth;
		}
	}

    void Update()
    {

        if (!active)
        {
            currCountdownValue -= 1 * Time.deltaTime;
            if (currCountdownValue <= 0)
            {
                active = true;
                GetComponent<BoxCollider>().enabled = true;
                GetComponent<MeshRenderer>().enabled = true;
            }

        }
    }

    public void SetInactive()
    {
        active = false;
        GetComponent<BoxCollider>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;
        currCountdownValue = countdownValue;
    }
}