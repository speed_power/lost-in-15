﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour {

    [SerializeField]
    RectTransform compassTransform;

    [SerializeField]
    Transform playerTransform;

	void Start () {

	}
    
	void Update () {
        float angle = playerTransform.rotation.eulerAngles.y;
        //Debug.Log(angle);

        compassTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
