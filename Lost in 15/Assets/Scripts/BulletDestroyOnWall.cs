﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroyOnWall : MonoBehaviour {

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Wall" || collider.tag == "Floor")
        {
            PoolObject po = GetComponent<PoolObject>();
            po.Recycle();
        }
    }
}
