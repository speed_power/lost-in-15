﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    [SerializeField]
    private float sensibility;
	void Update ()
    {
        if (Time.timeScale!=0)
        {
            transform.Rotate(0, (Input.GetAxis("Mouse X") * sensibility), 0);
        }
	}
}
