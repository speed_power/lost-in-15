﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    [SerializeField]
    float velocity;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		float inputhorizontal = Input.GetAxis ("Horizontal");
		float xtranslation = inputhorizontal * Time.deltaTime * velocity; //numero hardcodeado para la velocidad. cambiar //movimiento en X del personaje

		float inputVertical = Input.GetAxis ("Vertical");
		float ztranslation = inputVertical * Time.deltaTime * velocity; //numero hardcodeado para la velocidad. cambiar //movimiento en Z del personaje

		transform.Translate(new Vector3(xtranslation, 0 , ztranslation));

	}
    Vector3 GetPosition()
    {
        return this.transform.position;
    }
}
