﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxRenderer : MonoBehaviour
{
    MeshRenderer theRenderer;
    BoxRenderer2 childRender;
    void Start ()
    {
        theRenderer = GetComponent<MeshRenderer>();
        childRender = GetComponentInChildren<BoxRenderer2>();
	}
	public void MeshOn(bool on)
    {
        if(on)
        {
            theRenderer.enabled = true;
            childRender.MeshOn(true);
        }
        else
        {
            theRenderer.enabled = false;
            childRender.MeshOn(false);
        }
    }
}
