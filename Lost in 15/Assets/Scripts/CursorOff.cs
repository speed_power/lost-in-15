﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorOff : MonoBehaviour {

	// Use this for initialization
	void Start () {
    }
	
	public void HideCursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
