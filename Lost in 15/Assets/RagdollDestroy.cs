﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollDestroy : MonoBehaviour {
    
	void Start ()
    {
        Invoke("Destruction", 3.0f);
	}

    void Destruction()
    {
        Destroy(gameObject);
    }
}
